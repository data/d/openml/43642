# OpenML dataset: Plz-predict-these-disasters-to-save-lives-around.

https://www.openml.org/d/43642

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
As a community of responsible people can we focus to predict these disasters to save lives around. People are already going through worse as if that was not enough if a sudden earthquake comes up it becomes hell for them.
The United States Geological Survey (USGS) determines the location and size of all significant earthquakes that occur in US.The USGS provides science about the natural hazards that threaten lives and livelihoods; the water, energy, minerals, and other natural resources we rely on; the health of our ecosystems and environment; and the impacts of climate and land-use change.
Established: 1879 Location: Reston, United St
Content
time     latitude    longitude   depth   mag magType nst gap dmin    rms net id  updated place   type    horizontalError depthError  magError    magNst  status  locationSource  magSource
1.)time
Data Type-Long Integer
The time when the event occurred. Times are reported in milliseconds since the epoch ( 1970-01-01T00:00:00.000Z), and do not include leap seconds. In certain output formats, the date is formatted for readability.(We provide time in UTC (Coordinated Universal Time). Seismologists use UTC to avoid confusion caused by local time zones and daylight savings time.)
Additional Information
2.)latitude
Data Type-Decimal
Typical Values-[-90.0, 90.0]
Decimal degrees latitude. Negative values for southern latitudes.
Additional Information
An earthquake begins to rupture at a hypocenter which is defined by a position on the surface of the earth (epicenter) and a depth below this point (focal depth). We provide the coordinates of the epicenter in units of latitude and longitude. The latitude is the number of degrees north (N) or south (S) of the equator and varies from 0 at the equator to 90 at the poles. The longitude is the number of degrees east (E) or west (W) of the prime meridian which runs through Greenwich, England. The longitude varies from 0 at Greenwich to 180 and the E or W shows the direction from Greenwich. Coordinates are given in the WGS84 reference frame. The position uncertainty of the hypocenter location varies from about 100 m horizontally and 300 meters vertically for the best located events, those in the middle of densely spaced seismograph networks, to 10s of kilometers for global events in many parts of the world.
3.)longitude
Data Type-Decimal
Typical Values-[-180.0, 180.0]
Description-Decimal degrees longitude. Negative values for western longitudes.
Additional Information
An earthquake begins to rupture at a hypocenter which is defined by a position on the surface of the earth (epicenter) and a depth below this point (focal depth). We provide the coordinates of the epicenter in units of latitude and longitude. The latitude is the number of degrees north (N) or south (S) of the equator and varies from 0 at the equator to 90 at the poles. The longitude is the number of degrees east (E) or west (W) of the prime meridian which runs through Greenwich, England. The longitude varies from 0 at Greenwich to 180 and the E or W shows the direction from Greenwich. Coordinates are given in the WGS84 reference frame. The position uncertainty of the hypocenter location varies from about 100 m horizontally and 300 meters vertically for the best located events, those in the middle of densely spaced seismograph networks, to 10s of kilometers for global events in many parts of the world.
4.)depth
Data Type-Decimal
Typical Values-[0, 1000]
Depth of the event in kilometers.
Additional Information
Sometimes when depth is poorly constrained by available seismic data, the location program will set the depth at a fixed value. For example, 33 km is often used as a default depth for earthquakes determined to be shallow, but whose depth is not satisfactorily determined by the data, whereas default depths of 5 or 10 km are often used in mid-continental areas and on mid-ocean ridges since earthquakes in these areas are usually shallower than 33 km.
5.)mag
Data Type-Decimal
Typical Values-[-1.0, 10.0]
Description-The magnitude for the event. See also magType.
Additional Info
6.)magType
Data Type-String
Typical Values-Md, Ml, Ms, Mw, Me, Mi, Mb, MLg
The method or algorithm used to calculate the preferred magnitude for the event.
Additional Information
See Magnitude Types Table.
7.)nst
Data Type-Integer
The total number of seismic stations used to determine earthquake location.
Additional Information
Number of seismic stations which reported P- and S-arrival times for this earthquake. This number may be larger than the Number of Phases Used if arrival times are rejected because the distance to a seismic station exceeds the maximum allowable distance or because the arrival-time observation is inconsistent with the solution.
8.)gap
Data Type-Decimal
Typical Values-[0.0, 180.0]
The largest azimuthal gap between azimuthally adjacent stations (in degrees). In general, the smaller this number, the more reliable is the calculated horizontal position of the earthquake. Earthquake locations in which the azimuthal gap exceeds 180 degrees typically have large location and depth uncertainties.
9.)dmin
Data Type-Decimal
Typical Values-[0.4, 7.1]
Horizontal dis distance from the epicentre to the nearest station (in degrees). 1 degree is approximately 111.2 kilometers. In general, the smaller this number, the more reliable is the calculated depth of the earthquake.
10.)rms
Data Type-Decimal
Typical Values-[0.13,1.39]
The root-mean-square (RMS) travel time residual, in sec, using all weights. This parameter provides a measure of the fit of the observed arrival times to the predicted arrival times for this location. Smaller numbers reflect a better fit of the data. The value is dependent on the accuracy of the velocity model used to compute the earthquake location, the quality weights assigned to the arrival time data, and the procedure used to locate the earthquake.
11.)net
Data Type-String
Typical Values-ak, at, ci, hv, ld, mb, nc, nm, nn, pr, pt, se, us, uu, uw
The ID of a data contributor. Identifies the network considered to be the preferred source of information for this event.
12.)id
Data Type-String
Typical Values-A (generally) two-character network identifier with a (generally) eight-character network-assigned code.
A unique identifier for the event. This is the current preferred id for the event, and may change over time. See the "ids" GeoJSON format property.
13.)updated
Data Type-Long Integer
Time when the event was most recently updated. Times are reported in milliseconds since the epoch. In certain output formats, the date is formatted for readability.
14.)place
Data Type-String
Textual description of named geographic region near to the event.
15.)type
Data Type-String
Typical Values-earthquake, quarry
Type of seisseismic event.
16.)horizontalError
Data Type-Decimal
Typical Values-[0, 100]
Uncertainty of reported location of the event in kilometers.
Additional Information
The horizontal location error, in km, defined as the length of the largest projection of the three principal errors on a horizontal plane. The principal errors are the major axes of the error ellipsoid and are mutually perpendicular. The horizontal and vertical uncertainties in an event's location varie from about 100 m horizontally and 300 meters vertically for the best-located events, those in the middle of densely spaced seismograph networks, to 10s of kilometers  for global events in many parts of the world. We report an "unknown" value if the contributing seismic network does not supply uncertainty estimates.
17.)depthError
Data Type-Decimal
Typical Values-[0, 100]
Uncertainty of reported depth of the event in kilometers.
Additional Information
The depth error, in km, defined as the largest projection of the three principal errors on a vertical line.
18.)magError
Data Type-Decimal
Typical Values-[0, 100]
Uncertainty of reported magnitude of the event. The estimated standard error of the magnitude. The uncertainty corresponds to the specific magnitude type being reported and does not take into account magnitude variations and biases between different magnitude scales. We report an "unknown" value if the contributing seismic network does not supply uncertainty estimates.
19.)magNst
Data Type-Integer
The total number of seismic stations used to calculate the magnitude for this earthquake.
20.)status
Data Type-String
Typical Values-automatic, reviewed, deleted
Indicates whether the event has been reviewed by a human.
Additional Information
Status is either automatic or reviewed. Automatic events are directly posted by automatic processing systems and have not been verified or altered by a human. Reviewed events have been looked at by a human. The level of review can range from a quick validity check to a careful reanalysis of the event.
21.)locationSource
Data Type-String
Typical Values-(ak, at, ci, hv, ld, mb, nc, nm, nn, pr, pt, se, us, uu, uw)
The network that originally authored the reported location of this event.
22.)magSource
Data Type-String
Typical Values-(ak, at, ci, hv, ld, mb, nc, nm, nn, pr, pt, se, us, uu, uw)
The network that originally authored the reported magnitude for this event.
Inspiration
We are facing all kinds of disasters in this year 2020.Can we join as community and work towards atlest getting some predication from this dataset that contains many important factors and inspire the people by sharing the possibility of Data Science as a community.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43642) of an [OpenML dataset](https://www.openml.org/d/43642). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43642/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43642/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43642/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

